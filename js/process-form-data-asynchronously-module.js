/*!
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * Process Form Data Asynchronously Module v0.1.0
 *
 * This module contains a function that can be used to process an asynchronous request with a provided callback for
 * handling a response.
 *
 * by Ryan E. Anderson
 *
 * Copyright (C) 2020 Ryan E. Anderson
 */
function processFormDataAsynchronously(window, form, action, method, accept = "application/json", callback) {
    if (!(window instanceof Window))
        throw "The window must be an instance of Window.";

    if (!Object.prototype.isPrototypeOf.call(window.HTMLFormElement.prototype, form))
        throw "The value of form must be an HTML form.";

    if (form.getAttribute("action"))
        action = form.getAttribute("action");
    else if (typeof action !== "string")
        throw "The value of action must be a string.";

    if (form.getAttribute("method"))
        method = form.getAttribute("method");
    else if (typeof method !== "string" || method.toUpperCase() !== "POST" && method.toUpperCase() !== "GET")
        throw "A form method of either POST or GET must be provided.";

    const request = new XMLHttpRequest();
    const response = {
        data: null,
        status: "ok"
    };
    const payload = new FormData(form);
    const sanitizationObject = {
        slashRemovalCount: 0,
        sanitizedUrl: null
    };

    let currentUrl;
    let searchParameters;

    request.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE) {
            if (this.status !== 200)
                response.status = "error";
            else
                response.data = request.responseText;

            if (callback)
                callback(response);
        }
    };

    if (action.startsWith("http") || action.startsWith("https")) { // Transform the action so that it can be sanitized.
        currentUrl = new URL(action);

        action = `${currentUrl.pathname}${currentUrl.search}`; // Assign a path to the action variable so that it can be sanitized.
    } else {
        if (!action.startsWith("/")) {
            action = `/${action}`;
        }

        currentUrl = window.location;
    }

    let urlArray = action.split("");
    let index = -1;

    while (urlArray[++index] && urlArray[index] !== "?" && urlArray[index] !== "#")
        while (urlArray[index] === "/" && urlArray[index + 1] === "/")
            sanitizationObject.slashRemovalCount += urlArray.splice(index++, 1, "").length; // For sanitization purposes, remove all duplicate slashes from the path.

    action = urlArray.join("");

    currentUrl = new URL(`${currentUrl.protocol}//${currentUrl.hostname}${action}`); // Create a new URL object with the sanitized path.

    if (method.toUpperCase() === "GET") {
        const payloadEntries = [...payload.entries()];
        const transformedPayload = payloadEntries
            .map(entry => `${encodeURIComponent(entry[0])}=${encodeURIComponent(entry[1].toString())}`)
            .join("&");

        searchParameters = new URLSearchParams(transformedPayload);
    } else {
        searchParameters = new URLSearchParams(currentUrl.search);

        currentUrl = new URL(`${currentUrl.protocol}//${currentUrl.hostname}${currentUrl.pathname}`);
    }

    [...searchParameters.entries()].forEach(entry => {
        currentUrl.searchParams.append(entry[0], entry[1]);
    });

    sanitizationObject.sanitizedUrl = currentUrl;

    action = currentUrl.href;

    request.open(method, action, true);
    request.setRequestHeader("Accept", accept);

    if (method.toUpperCase() === "GET")
        request.send();
    else
        request.send(payload);

    return sanitizationObject;
}

export default processFormDataAsynchronously;