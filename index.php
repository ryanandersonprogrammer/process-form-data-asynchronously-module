<!DOCTYPE html>
<!--
Copyright 2020 Ryan E. Anderson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

<!--
    Process Form Data Asynchronously Module v0.1.0 Index

    by Ryan E. Anderson

    Copyright (C) 2020 Ryan E. Anderson
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <link rel="icon" type="image/png" sizes="16x16"
          href="media/png/Process-Form-Data-Asynchronously-Module-Icon-16.png">
    <link rel="icon" type="image/png" sizes="32x32"
          href="media/png/Process-Form-Data-Asynchronously-Module-Icon-32.png">
    <link rel="icon" type="image/png" sizes="64x64"
          href="media/png/Process-Form-Data-Asynchronously-Module-Icon-64.png">
    <!--[if IE]>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
    <![endif]-->
    <link rel="stylesheet" href="css/process-form-data-asynchronously-module-demo-style.css" type="text/css">

    <title>Process Form Asynchronously Module - Demo</title>
</head>
<body>
<div id="wrapper">
    <header>
        <div class="text-container">
            <h1 class="golden-line-height"><a href="/">Process Form Data Asynchronously Module Demo</a></h1>
        </div>
        <div class="text-container">
            <h2 class="golden-line-height">v0.1.0</h2>
        </div>
        <div class="text-container">
            <p class="golden-line-height">
                By Ryan Anderson
            </p>
        </div>
        <div class="text-container">
            <p class="golden-line-height">
                <small>Published
                    <time>2020-12-31</time>
                </small>
            </p>
        </div>
    </header>
    <main>
        <section>
            <div class="text-container">
                <h3 class="golden-line-height title-body">Summing Two Addends</h3>
            </div>
            <div class="text-container">
                <p class="golden-line-height text-body">Find the sum of two addends.</p>
            </div>
            <article>
                <div class="text-container">
                    <h4 class="golden-line-height title-body">Form</h4>
                </div>
                <div class="text-container">
                    <p class="golden-line-height text-body">
                        Use the form to enter two addends that will be processed upon submission to produce a sum.
                    </p>
                </div>
                <form id="add-stuff-form" class="form">
                    <div class="text-container">
                        <h2 class="golden-line-height title-body">Add Stuff Form</h2>
                    </div>
                    <div class="text-container">
                        <p class="golden-line-height text-body">
                            Submit the form to asynchronously request a sum of the two numbers that were provided as
                            input.
                        </p>
                    </div>
                    <fieldset>
                        <legend class="title-body golden-line-height">Create Input Form</legend>
                        <fieldset>
                            <legend class="title-body golden-line-height">Parameters</legend>
                            <div id="input-number-addend-group-1"
                                 class="form-group">
                                <label for="input-number-addend-1" class="golden-line-height">Input Number Addend
                                    1:</label>
                                <input type="number"
                                       id="input-number-addend-1"
                                       name="input-number-addend-1"
                                       value="0"
                                       min="0"
                                       max="100"
                                       placeholder="enter an integer greater than or equal to 0 and less than or equal to 100"
                                       title="This is input number addend 1."
                                       required/>
                                <div class="clear-fix"></div>
                            </div>
                            <div id="input-number-addend-group-2"
                                 class="form-group">
                                <label for="input-number-addend-2" class="golden-line-height">Input Number Addend
                                    2:</label>
                                <input type="number"
                                       id="input-number-addend-2"
                                       name="input-number-addend-2"
                                       value="0"
                                       min="0"
                                       max="100"
                                       placeholder="enter an integer greater than or equal to 0 and less than or equal to 100"
                                       title="This is input number addend 2."
                                       required/>
                                <div class="clear-fix"></div>
                            </div>
                        </fieldset>
                        <div class="form-control">
                            <div class="form-button-wrapper">
                                <button id="clear-button"
                                        type="button"
                                        class="form-button-right tertiary-button golden-line-height"
                                        onclick="clearFormAndOutput()">Clear
                                </button>
                            </div>
                            <div class="form-button-wrapper">
                                <button id="reset-button"
                                        type="reset"
                                        class="form-button-right secondary-button golden-line-height">Reset
                                </button>
                            </div>
                            <div class="form-button-wrapper">
                                <button id="submit-button"
                                        type="submit"
                                        class="form-button-right primary-button golden-line-height">Submit
                                </button>
                            </div>
                        </div>
                    </fieldset>
                    <div class="text-container">
                        <p class="fine-print fine-print-center golden-line-height text-body">
                            <small><em>Note: This form will sum two addends between 0 and 100 inclusive.</em></small>
                        </p>
                    </div>
                    <div class="text-container">
                        <p class="fine-print fine-print-center golden-line-height text-body">
                            <small>Copyright &copy; 2020 Ryan E. Anderson</small>
                        </p>
                    </div>
                </form>
                <div id="output-wrapper" class="hide">
                    <div class="relative">
                        <p class="golden-line-height text-body"><a href="#output-wrapper">Close</a></p>
                    </div>
                    <div class="text-container">
                        <h4 class="golden-line-height title-body">Output</h4>
                        <pre id="output" class="golden-line-height"></pre>
                        <p id="sanitization-output" class="golden-line-height text-body">The number of slashes that were
                            removed is <span class="success-background">0</span>.</p>
                    </div>
                </div>
            </article>
        </section>
    </main>
    <footer>
        <div class="text-container">
            <p class="fine-print golden-line-height text-body">
                <small>
                    Copyright &copy; 2020 Ryan E. Anderson
                </small>
            </p>
        </div>
    </footer>
</div>
<script>
    function hideOutput(preIdentifier, outputWrapperIdentifier, showClass, hideClass) {
        const output = document.querySelector(preIdentifier);
        const outputWrapper = document.querySelector(outputWrapperIdentifier);

        const outputParentClassList = outputWrapper.classList;

        toggleClassList(outputParentClassList, hideClass, showClass);

        output.innerHTML = "";
    }

    function showOutput(preIdentifier, outputWrapperIdentifier, showClass, hideClass, content) {
        const output = document.querySelector(preIdentifier);
        const outputWrapper = document.querySelector(outputWrapperIdentifier);

        const outputParentClassList = outputWrapper.classList;

        toggleClassList(outputParentClassList, showClass, hideClass);

        output.innerHTML = content;
    }

    function toggleClassList(classList, addClass, removeClass) {
        if (classList.contains(removeClass)) {
            classList.remove(removeClass);

            classList.add(addClass);
        }
    }

    function removeClassFromClassList(classList, removeClass) {
        if (classList.contains(removeClass)) {
            classList.remove(removeClass);
        }
    }

    function addClassToClassList(classList, addClass) {
        if (!classList.contains(addClass)) {
            classList.add(addClass);
        }
    }

    function clearFormAndOutput() {
        const form = document.querySelector("form");
        const valueInputs = form.querySelectorAll("input[type='number']");
        const valueInputCount = valueInputs.length;

        for (let i = 0; i < valueInputCount; i++) {
            valueInputs[i].value = "";
        }

        hideOutput("#output", "#output-wrapper", "show", "hide");
    }
</script>
<script type="module">
    import processFormDataAsynchronously from "./js/process-form-data-asynchronously-module";

    document.addEventListener("DOMContentLoaded", () => {
        const form = document.querySelector("#add-stuff-form");
        const outputWrapperLink = document.querySelector("#output-wrapper > .relative > p > a");
        const resetButton = document.querySelector("#reset-button");
        const hideOutputCallback = () => {
            hideOutput("#output", "#output-wrapper", "show", "hide");
        };

        form.addEventListener("submit", (event) => {
            event.preventDefault();

            try {
                const url = "///////////add_stuff_service.php?constant-1=a&constant-2=b&&&&&&&&";
                const method = "GET";
                const contentType = "application/json";
                const callback = (response) => {
                    const data = JSON.parse(response.data);

                    showOutput("#output", "#output-wrapper", "show", "hide", JSON.stringify(data, null, 2));
                };
                const sanitizationOutput = document.querySelector("#sanitization-output > span");
                const sanitizationObject = processFormDataAsynchronously(window, form, url, method, contentType, callback);
                const sanitizationOutputClassList = sanitizationOutput.classList;

                let addClass;
                let removeClass;

                if (sanitizationObject.slashRemovalCount > 0) {
                    addClass = "warning-background";
                    removeClass = "success-background";

                    removeClassFromClassList(sanitizationOutputClassList, "white-foreground");

                    sanitizationOutput.innerText = sanitizationObject.slashRemovalCount;
                } else {
                    addClass = "success-background";
                    removeClass = "warning-background";

                    addClassToClassList(sanitizationOutputClassList, "white-foreground");

                    sanitizationOutput.innerText = 0;
                }

                toggleClassList(sanitizationOutputClassList, addClass, removeClass);
            } catch (error) {
                showOutput("#output", "#output-wrapper", "show", "hide", error);
            }
        });
        outputWrapperLink.addEventListener("click", hideOutputCallback);
        resetButton.addEventListener("click", hideOutputCallback);
    });
</script>
</body>
</html>