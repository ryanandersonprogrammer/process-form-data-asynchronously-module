# Process Form Data Asynchronously Module

## About

### Author

Ryan E. Anderson

---

### Description

The Process Form Data Asynchronously Module is a JavaScript module that can be used to simplify asynchronous processing 
of form data.

- The Process Form Data Asynchronously Module exports a function that uses the XMLHttpRequest type to send asynchronous 
requests.
- The Process Form Data Asynchronously Module exports a function that allows a user to pass a custom callback function 
to handle a response.
- The Process Form Data Asynchronously Module exports a function that will sanitize the action of a form and return an 
object that contains information about how the original URL was changed.

---

### Version

0.1.0

---

### License

Apache-2.0

---

## processFormDataAsynchronously

### Parameters

The processFormDataAsynchronously() function has six parameters, one of which has a default value. Each parameter will 
be described below.

#### window

This parameter is used to pass the global Window object of a certain browser context as an argument.

#### form

This parameter is used to pass a form element from the DOM so that its data can be processed. During each request that 
passes validation, the FormData type is used to conveniently manipulate and prepare the data of the form before 
transmission.

#### action

This parameter specifies the URL or path that will be used to send an asynchronous request. If a hostname is not 
included in the URL, then that of the window will be used. The search parameters stored in the URL of the window will 
not be used if a hostname is not provided. Also, during each request that passes validation, the path name of the action
is sanitized before the transmission of data.

#### method

This parameter specifies the HTTP method that will be used to send an asynchronous request. A method that is specified 
in a form has precedence over that of the argument list of processFormDataAsynchronously(). If the method attribute of a 
form is set, then its value of either post or get will be used; otherwise, the method argument that is passed to 
processFormDataAsynchronously() will determine how a request will be sent.

#### accept

This parameter specifies the format of the data that will be transmitted during an asynchronous request. The value of 
any argument passed to this parameter will be used to specify an Accept request HTTP header. If a value is not passed as 
an argument, then application/json will be used.

#### callback

This parameter accepts a custom function that can be used to handle a response.

### Output

The processFormDataAsynchronously() function returns an object that contains information about the sanitization that 
took place during a request. The sanitization object stores the number of slashes that were removed from a path 
argument. Also, the sanitization object stores a URL object that contains the final state of the path before data 
transmission.

## Using the Process Form Data Asynchronously Module in a Project

### Installation

Because the process-form-data-asynchronously-module.js file exports a module, it should be used with a script element of 
type module. Below is sample markup of a script element containing the use of the module (See demo.).

```html
<script type="module">
    import processFormDataAsynchronously from "./js/process-form-data-asynchronously-module";

    document.addEventListener("DOMContentLoaded", () => {
        const form = document.querySelector("#add-stuff-form");
        const outputWrapperLink = document.querySelector("#output-wrapper > .relative > p > a");
        const resetButton = document.querySelector("#reset-button");
        const hideOutputCallback = () => {
            hideOutput("#output", "#output-wrapper", "show", "hide");
        };

        form.addEventListener("submit", (event) => {
            event.preventDefault();

            try {
                const url = "///////////add_stuff_service.php?constant-1=a&constant-2=b&&&&&&&&";
                const method = "GET";
                const contentType = "application/json";
                const callback = (response) => {
                    const data = JSON.parse(response.data);

                    showOutput("#output", "#output-wrapper", "show", "hide", JSON.stringify(data, null, 2));
                };
                const sanitizationOutput = document.querySelector("#sanitization-output > span");
                const sanitizationObject = processFormDataAsynchronously(window, form, url, method, contentType, callback);
                const sanitizationOutputClassList = sanitizationOutput.classList;

                let addClass;
                let removeClass;

                if (sanitizationObject.slashRemovalCount > 0) {
                    addClass = "warning-background";
                    removeClass = "success-background";

                    removeClassFromClassList(sanitizationOutputClassList, "white-foreground");

                    sanitizationOutput.innerText = sanitizationObject.slashRemovalCount;
                } else {
                    addClass = "success-background";
                    removeClass = "warning-background";

                    addClassToClassList(sanitizationOutputClassList, "white-foreground");

                    sanitizationOutput.innerText = 0;
                }

                toggleClassList(sanitizationOutputClassList, addClass, removeClass);
            } catch (error) {
                showOutput("#output", "#output-wrapper", "show", "hide", error);
            }
        });
        outputWrapperLink.addEventListener("click", hideOutputCallback);
        resetButton.addEventListener("click", hideOutputCallback);
    });
</script>
```

## Demo

A demo is provided to evaluate the module. To use the demo, deploy all contents from the root of this project to a 
directory on a web server that supports PHP; and make any necessary mappings for virtual hosts.

### Support

The markup of the demo is supported by legacy browsers; however, newer JavaScript features won't be supported in some 
older contexts because of the version of ECMAScript that is used in the implementation of the module and in other 
scripts.