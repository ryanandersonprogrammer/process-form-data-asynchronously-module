<?php
/**
 * Copyright 2020 Ryan E. Anderson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Process Form Data Asynchronously Module v0.1.0 Add Stuff Service Script
 *
 * by Ryan E. Anderson
 *
 * Copyright (C) 2020 Ryan E. Anderson
 */
$result = [];
$messages = [];

try {
    switch ($_SERVER["REQUEST_METHOD"]):
        case "GET":
            $request = &$_GET;
            break;
        case "POST":
            $request = &$_POST;
            break;
        default:
            throw new Exception("The request that was sent is not supported.");
    endswitch;

    if (isset($request["input-number-addend-1"]) && isset($request["input-number-addend-2"])):
        $first_addend = $request["input-number-addend-1"];
        $second_addend = $request["input-number-addend-2"];

        if (is_numeric($first_addend) && is_numeric($second_addend)):
            $sum = $first_addend + $second_addend;

            $result["sum"] = $sum;

            $message = [
                "description" => "The request succeeded. The sum was calculated.",
                "type" => "success"
            ];

            array_push($messages, $message);
        else:
            $message = [
                "description" => "The value of each addend must be an integer.",
                "type" => "error"
            ];

            array_push($messages, $message);
        endif;
    else:
        $message = [
            "description" => "The request must contain data with expected keys.",
            "type" => "error"
        ];

        array_push($messages, $message);
    endif;
} catch (Exception $exception) {
    $message = [
        "description" => $exception->getMessage(),
        "type" => "error"
    ];

    array_push($messages, $message);
}

$type_count_array = array_count_values(array_column($messages, "type"));

$response = [
    "response" => [
        "result" => $result,
        "status" => isset($type_count_array["error"]) && $type_count_array["error"] > 0 ? "error" : "success",
        "messages" => $messages
    ]
];

header("Content-Type: application/json");

echo json_encode($response);
